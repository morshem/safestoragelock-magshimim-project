package com.example.roy.project;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class ConnectActivity extends AppCompatActivity {
    private static String BROADCAST_MESSAGE; //the message to send with the broadcast (user's username)
    private static final int PORT = 5001;
   //private ListView computerList;
    private final List<String> computerRecordList = new ArrayList<>();
    private int pickedComputerIndex;
    private ArrayAdapter<String> adapter; // Create the adapter for the computer list
    private final Handler threadHandler = new Handler(); //syncs the UI thread and the network thread



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle b = getIntent().getExtras();
        BROADCAST_MESSAGE = b.getString("username");
        BROADCAST_MESSAGE = '@' + BROADCAST_MESSAGE + "||"; //get the username from the previous activity


        populateListView(); //make the computer list
        registerClickCallback(); //listen for clicks one of the computers

        new MyAsyncTask().execute(); //send broadcast
        final BroadcastReceiver broadcastReceiver = new BroadcastReceiver(); //receive broadcast
        broadcastReceiver.start();

        threadHandler.postDelayed(new Runnable() { //delay the UI thread until we receive an answer
            public void run() {
                int len = broadcastReceiver.GetQueueLength();
                for(int i = 0; i < len; i++) {
                    runOnUiThread(new Runnable() { //construct the computer list
                        @Override
                        public void run() {
                            String lastMessage = broadcastReceiver.GetLastMessage();
                            adapter.add(lastMessage);
                        }
                    });
                }
            }
        }, 11000);
    }

    @Override
    protected void onPause(){ //when we go back to the previous activity, destroy this one
        super.onPause();
        finish();
    }

    public class MyAsyncTask extends AsyncTask<String, Integer, Long> {

        @Override
        protected Long doInBackground(String... params) { //in another thread, send the broadcast
            sendBroadcast();
            return 6L;
        }


        public void sendBroadcast() {
            try {
                DatagramSocket socket = new DatagramSocket();
                socket.setBroadcast(true);
                byte[] sendData = BROADCAST_MESSAGE.getBytes(); //BROADCAST_MESSAGE = username
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, InetAddress.getByName("255.255.255.255"), PORT); //send broadcast to everybody
                socket.send(sendPacket);
                socket.close();
            } catch (IOException e) {
                Log.e("ConnectActivity", "IOException: " + e.getMessage());
            }
        }

    }


    private void populateListView() {
        adapter = new ArrayAdapter<String>( // Build Adapter
                this, // Context for the activity
                R.layout.computer_list_row_layout, // Layout to use (create)
                computerRecordList); // adapter
        // Configure the list view
        ListView list = (ListView) findViewById(R.id.computerListView);
        list.setAdapter(adapter);
    }

    private void registerClickCallback() {
        ListView list = (ListView) findViewById(R.id.computerListView);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView parent, View viewClicked, int position, long id) {
                pickedComputerIndex = position;
                AlertDialog.Builder builder = new AlertDialog.Builder(ConnectActivity.this);
                builder.setMessage("Do you want to connect to " + computerRecordList.get(position) +"?").setPositiveButton("Yes", dialogClickListener)
                        .setNegativeButton("No", dialogClickListener).show();
            }
        });
    }

    private final DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    //Connect to selected computer
                    Intent intent = new Intent(((AlertDialog) dialog).getContext(), PasswordActivity.class);

                    String name = computerRecordList.get(pickedComputerIndex);
                    name = name.substring(0, name.indexOf(" |"));

                    String address = computerRecordList.get(pickedComputerIndex);
                    address = address.substring(address.indexOf("| ") + 2);

                    NetworkHandler networkHandler = new NetworkHandler(address, "@100||", 1); //confirm the connection
                    networkHandler.start();

                    intent.putExtra("address", address);
                    intent.putExtra("name", name);
                    startActivity(intent);
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };
}