package com.example.roy.project;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class PasswordActivity extends AppCompatActivity {

    private String address;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Bundle b = getIntent().getExtras();
        address = b.getString("address");
        name = b.getString("name");

        TextView connectedTo = (TextView) findViewById(R.id.connectedToTextView);
        String connectedToString = "Connected To " + "<b>" + name + "<b>";
        connectedTo.setText(Html.fromHtml(connectedToString));

    }

    public void sendPassword(View view) {
        EditText password = (EditText) findViewById(R.id.passwordEditText);
        String passwordString = password.getText().toString(); //get the password
        if (!passwordString.equals("")) {
            passwordString = "@105|" + passwordString + "||"; //put it in the right message protocol

            password.setText(""); //reset the password field

            NetworkHandler networkHandler = new NetworkHandler(address, passwordString, 1); //send the password
            networkHandler.start();
        }
    }
}
