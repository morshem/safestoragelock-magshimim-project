package com.example.roy.project;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

public class UsernameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_username);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    public void ConfirmUsername(View view) {
        EditText username = (EditText) findViewById(R.id.usernameEditText);
        String usernameString = username.getText().toString(); //get the text from the username field

        if(!usernameString.equals("")) {
            Intent intent = new Intent(this, ConnectActivity.class);
            intent.putExtra("username", usernameString);
            startActivity(intent);
        }
    }
}
