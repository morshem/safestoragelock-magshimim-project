package com.example.roy.project;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Roy on 04/05/2016.
 */
public class BroadcastReceiver extends Thread{
    private static ConcurrentLinkedQueue<String> messageQueue;
    private boolean bKeepRunning = true;


    public BroadcastReceiver() {
        messageQueue = new ConcurrentLinkedQueue<>();
    }

    private final int port = 5000;


    @Override
    public void run() {
        String message = "";
        SocketAddress serverAddress;
        byte[] lmessage = new byte[256];
        DatagramPacket packet = new DatagramPacket(lmessage, lmessage.length);
        DatagramSocket socket;
        SocketAddress socketAddress;
        String address;


        try {
            socket = new DatagramSocket(port);
            socket.setSoTimeout(10);
            int i = 0;
            while (bKeepRunning) {
                i++;
                if (i > 1000) // If 10 seconds pass, stop searching for computers
                    bKeepRunning = false;
                try {
                    socket.receive(packet);
                }
                catch  (SocketTimeoutException e) { // Continue once in every 10 miliseconds
                    continue;
                }

                message = new String(lmessage, 0, packet.getLength());
                if (!message.isEmpty()) {
                    message = message.substring(1, message.indexOf("||"));
                    socketAddress = packet.getSocketAddress();
                    address = socketAddress.toString();
                    address = address.substring(1, address.indexOf(':'));
                    String nameAndAddress = message + " | " + address; //conjoin the name and address
                    messageQueue.add(nameAndAddress);
                }

            }
            if (socket != null) {
                socket.close();
            }
        }
        catch (Throwable e) {
            e.printStackTrace();
            String s = e.getMessage();
        }
    }

    public String GetLastMessage() {
        if (!messageQueue.isEmpty()) {
            return messageQueue.remove();
        }
        return "";
    }

    public int GetQueueLength() {
        return messageQueue.size();
    }

}

