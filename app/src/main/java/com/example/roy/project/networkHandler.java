package com.example.roy.project;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Roy on 04/06/2016.
 */
public class NetworkHandler extends Thread {
    private static ConcurrentLinkedQueue<String> messageQueue;
    private InetAddress IP; //the IP address of the computer we want to send to / receive from
    private String message; //the message we send / the message we want to receive
    private int sendOrRecv; //1 = send, 2 = receive

    public NetworkHandler(String IP, String message, int sendOrRecv) {
        try {
            this.IP = InetAddress.getByName(IP);
            this.message = message;
            this.sendOrRecv = sendOrRecv;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        messageQueue = new ConcurrentLinkedQueue<>();
    }

    @Override
    public void run() {

        SocketAddress serverAddress;

        //receive variables
        byte[] lmessage = new byte[256];
        String gotMessage;
        DatagramPacket receivePacket = new DatagramPacket(lmessage, lmessage.length);
        DatagramSocket receiveSocket;
        int receivePort = 5000;

        //send variables
        byte[] sendData = message.getBytes();
        int sendPort = 5001;
        DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IP, sendPort);

        if(sendOrRecv == 1) //if we want to send
        {
            try {
                DatagramSocket sendSocket = new DatagramSocket();
                sendSocket.send(sendPacket);
                sendSocket.close();
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        else if (sendOrRecv == 2) { //if we want to receive
            while(true) {
                try {
                    receiveSocket = new DatagramSocket(receivePort);
                    receiveSocket.receive(receivePacket);
                    gotMessage = new String(lmessage, 0, receivePacket.getLength());

                    messageQueue.add(gotMessage);
                    if(gotMessage.equals(message)) { //if the message we got is what we expected, stop receiving
                        break;
                    }
                }
                catch (Throwable e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String GetLastMessage() {
        if (!messageQueue.isEmpty()) {
            return messageQueue.remove();
        }
        return "";
    }

}
